ENV['RACK_ENV'] = 'test'

require 'bundler/setup'
Bundler.require :default

require 'rack/test'
require File.expand_path '../../app.rb', __FILE__

disable :run

Capybara.app = ItaucardReportsApp

RSpec.configure do |config|
  config.include Capybara::DSL
end
