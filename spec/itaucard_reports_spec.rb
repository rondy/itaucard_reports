require 'spec_helper'
require './itaucard_reports'

describe ItaucardReports do
  describe ItaucardReports::CreditCardStatementParser, 'TRN' do
    let(:extract_transactions) do
      ItaucardReports::CreditCardStatementParser.new('fatura102015trn.html').call
    end

    it 'extracts 64 transactions from the given fixture file' do
      transactions = extract_transactions

      expect(transactions.size).to eq(59)
    end

    it 'extracts the date a transaction was made' do
      transactions = extract_transactions

      expect(transactions[0].date).to eq('16/12')
      expect(transactions[1].date).to eq('22/01')
    end

    it 'extracts the full title of a transaction (composed of title and installments)' do
      transactions = extract_transactions

      expect(transactions[0].full_title).to eq('FOTO PAULO        10/10')
      expect(transactions[1].full_title).to eq('GOL TRAN   SP AERE09/10')
    end

    it 'extracts the title of a transaction' do
      transactions = extract_transactions

      expect(transactions[0].title).to eq('FOTO PAULO')
      expect(transactions[1].title).to eq('GOL TRAN   SP AERE')
    end

    it 'strips trailing whitespaces from the title of a transaction' do
      transactions = extract_transactions

      expect(transactions[2].title).to eq('IPLACE')
      expect(transactions[3].title).to eq('SGH BRASIL')
    end

    it 'considers some non-word characters (i.g. "\'", "-", "." and "*" ) in the title of a transaction' do
      transactions = extract_transactions

      expect(transactions[4].title).to eq('PAGSEGURO*UltraMus')
      expect(transactions[7].title).to eq('AMAZON.COM.BR-')
      expect(transactions[16].title).to eq('ZARA-SHOP.ELDORADO')
    end

    it 'extracts the installments of a transaction' do
      transactions = extract_transactions

      expect(transactions[0].installments).to eq('10/10')
      expect(transactions[1].installments).to eq('09/10')
    end

    it 'returns no installments when the transaction is a in-cash purchase' do
      transactions = extract_transactions

      expect(transactions[19].installments).to be_nil
      expect(transactions[20].installments).to be_nil
    end

    it 'extracts the amount of a transaction' do
      transactions = extract_transactions

      expect(transactions[0].amount.to_f).to eq(71.9)
      expect(transactions[1].amount.to_f).to eq(31.95)
    end
  end

  describe ItaucardReports::CreditCardStatementParser do
    let(:extract_transactions) do
      ItaucardReports::CreditCardStatementParser.new('fatura112015.html').call
    end

    it 'extracts 64 transactions from the given fixture file' do
      transactions = extract_transactions

      expect(transactions.size).to eq(64)
    end

    it 'extracts the date a transaction was made' do
      transactions = extract_transactions

      expect(transactions[0].date).to eq('22/01')
      expect(transactions[1].date).to eq('20/04')
    end

    it 'extracts the full title of a transaction (composed of title and installments)' do
      transactions = extract_transactions

      expect(transactions[0].full_title).to eq('Gol Tran   Sp Aere10/10')
      expect(transactions[1].full_title).to eq('Iplace            07/10')
    end

    it 'extracts the title of a transaction' do
      transactions = extract_transactions

      expect(transactions[0].title).to eq('Gol Tran   Sp Aere')
    end

    it 'strips trailing whitespaces from the title of a transaction' do
      transactions = extract_transactions

      expect(transactions[1].title).to eq('Iplace')
      expect(transactions[3].title).to eq('Mercadopago')
    end

    it 'considers some non-word characters (i.g. "\'", "-", "." and "*" ) in the title of a transaction' do
      transactions = extract_transactions

      expect(transactions[4].title).to eq("Amazon.com.br'")
      expect(transactions[8].title).to eq('Zara-shop.eldorado')
      expect(transactions[12].title).to eq('Pagseguro*paseguro')
    end

    it 'extracts the installments of a transaction' do
      transactions = extract_transactions

      expect(transactions[0].installments).to eq('10/10')
      expect(transactions[1].installments).to eq('07/10')
    end

    it 'returns no installments when the transaction is a in-cash purchase' do
      transactions = extract_transactions

      expect(transactions[17].installments).to be_nil
      expect(transactions[18].installments).to be_nil
    end

    it 'extracts the amount of a transaction' do
      transactions = extract_transactions

      expect(transactions[0].amount.to_f).to eq(31.95)
      expect(transactions[1].amount.to_f).to eq(39.90)
    end
  end
end

describe ItaucardReports::TransactionItem do
  def build_transaction(transaction_hash)
    ItaucardReports::TransactionItem.new(transaction_hash)
  end

  let(:transaction_hash) do
    {
      credit: false,
      date: '14/11',
      full_title: 'Gol Tran   Sp Aere10/10',
      amount: '31,95'
    }
  end

  it 'returns the transaction date as it is from the transaction hash' do
    transaction = build_transaction(transaction_hash.merge(date: '14/11'))
    expect(transaction.date).to eq('14/11')

    transaction = build_transaction(transaction_hash.merge(date: '14/10'))
    expect(transaction.date).to eq('14/10')
  end

  it 'returns the transaction full title as it is from the transaction hash' do
    transaction = build_transaction(transaction_hash.merge(full_title: 'Gol Tran   Sp Aere10/10'))
    expect(transaction.full_title).to eq('Gol Tran   Sp Aere10/10')

    transaction = build_transaction(transaction_hash.merge(full_title: 'Gol Tran   Sp Aere'))
    expect(transaction.full_title).to eq('Gol Tran   Sp Aere')
  end

  it 'returns the transaction title from the full title' do
    transaction = build_transaction(transaction_hash.merge(full_title: 'Gol Tran   Sp Aere10/10'))
    expect(transaction.title).to eq('Gol Tran   Sp Aere')

    transaction = build_transaction(transaction_hash.merge(full_title: 'Gol Tran   Sp Aere'))
    expect(transaction.title).to eq('Gol Tran   Sp Aere')
  end

  it 'returns the transaction installments from the full title' do
    transaction = build_transaction(transaction_hash.merge(full_title: 'Gol Tran   Sp Aere10/10'))
    expect(transaction.installments).to eq('10/10')

    transaction = build_transaction(transaction_hash.merge(full_title: 'Gol Tran   Sp Aere'))
    expect(transaction.installments).to be_nil
  end

  it 'returns the transaction amount as decimal' do
    transaction = build_transaction(transaction_hash.merge(amount: '31,95'))
    expect(transaction.amount).to eq(31.95)

    transaction = build_transaction(transaction_hash.merge(amount: '0,5'))
    expect(transaction.amount).to eq(0.5)
  end

  it 'checks whether the transaction is a credit' do
    transaction = build_transaction(transaction_hash.merge(credit: false))
    expect(transaction.credit?).to be(false)

    transaction = build_transaction(transaction_hash.merge(credit: true))
    expect(transaction.credit?).to be(true)
  end

  it 'marks as in cash transaction if there is no installments' do
    transaction = build_transaction(transaction_hash.merge(full_title: 'Gol Tran   Sp Aere'))
    expect(transaction.in_cash?).to be(true)

    transaction = build_transaction(transaction_hash.merge(full_title: 'Gol Tran   Sp Aere10/10'))
    expect(transaction.in_cash?).to be(false)
  end

  it 'checks whether transaction is from the current month' do
    transaction = build_transaction(transaction_hash.merge(date: "01/#{Time.now.month}"))
    expect(transaction.current_month?).to be(true)

    transaction = build_transaction(transaction_hash.merge(date: "01/#{Time.now.to_date.next_month.month}"))
    expect(transaction.current_month?).to be(false)

    transaction = build_transaction(transaction_hash.merge(date: "01/#{Time.now.to_date.prev_month.month}"))
    expect(transaction.current_month?).to be(false)
  end

  it 'checks whether transaction is from the previous month' do
    transaction = build_transaction(transaction_hash.merge(date: "01/#{Time.now.month}"))
    expect(transaction.previous_month?).to be(false)

    transaction = build_transaction(transaction_hash.merge(date: "01/#{Time.now.to_date.next_month.month}",))
    expect(transaction.previous_month?).to be(false)

    transaction = build_transaction(transaction_hash.merge(date: "01/#{Time.now.to_date.prev_month.month}",))
    expect(transaction.previous_month?).to be(true)
  end

  it 'checks whether there will be installment in the next month' do
    transaction = build_transaction(transaction_hash.merge(full_title: 'Gol Tran   Sp Aere09/10'))
    expect(transaction.installment_in_next_month?).to be(true)

    transaction = build_transaction(transaction_hash.merge(full_title: 'Gol Tran   Sp Aere10/10'))
    expect(transaction.installment_in_next_month?).to be(false)

    transaction = build_transaction(transaction_hash.merge(full_title: 'Gol Tran   Sp Aere'))
    expect(transaction.installment_in_next_month?).to be(false)
  end

  it 'checks whether is a transaction from TIM' do
    transaction = build_transaction(transaction_hash.merge(full_title: 'Gol Tran   Sp Aere10/10'))
    expect(transaction.tim?).to be(false)

    transaction = build_transaction(transaction_hash.merge(full_title: 'Tim Internet*2mnsi7'))
    expect(transaction.tim?).to be(true)

    transaction = build_transaction(transaction_hash.merge(full_title: 'Pcte Adic 300Mb*2nt6ot'))
    expect(transaction.tim?).to be(true)

    transaction = build_transaction(transaction_hash.merge(full_title: 'TIM CTRWHATSAPP*2kuo44'))
    expect(transaction.tim?).to be(true)

    transaction = build_transaction(transaction_hash.merge(full_title: 'TIM INTERNET*2kb6rj'))
    expect(transaction.tim?).to be(true)
  end

  it 'checks whether is a transaction from Uber' do
    transaction = build_transaction(transaction_hash.merge(full_title: 'Gol Tran   Sp Aere10/10'))
    expect(transaction.uber?).to be(false)

    transaction = build_transaction(transaction_hash.merge(full_title: 'Uber*uber'))
    expect(transaction.uber?).to be(true)

    transaction = build_transaction(transaction_hash.merge(full_title: 'UBER*UBER'))
    expect(transaction.uber?).to be(true)
  end
end
