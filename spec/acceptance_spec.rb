require 'spec_helper'

describe 'Itaucard reports root page' do
  it 'User can see parsed information from a credit card statement' do
    visit '/'

    expect(page).to have_content('Itaucard Reports')
    expect(page).to have_content('64 transações nacionais (total: 1748.98)')
    expect(page).to have_content('39 transações à vista (total: 853.58)')
    expect(page).to have_content('25 transações parceladas (total: 895.4)')
    expect(page).to have_content('20 transações parceladas próximo mês (total: 698.19)')
    expect(page).to have_content('9 transações do mês atual (total: 162.43)')
    expect(page).to have_content('40 transações do mês anterior (total: 1036.25)')
    expect(page).to have_content('14 transações no Uber (total: 344.98)')
    expect(page).to have_content('5 transações na TIM (total: 92.5)')
  end
end
