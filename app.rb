require './itaucard_reports'
require 'bigdecimal'

class ItaucardReportsApp < Sinatra::Base
  set :root, File.dirname(__FILE__)

  get '/:filename' do
    raise ArgumentError, 'bad filename' unless params[:filename] =~ /\Afatura\d{6}(trn)?\.html\z/
    raise ArgumentError, 'file does not exist' unless File.exists?(params[:filename])

    @report = ItaucardReports.parse_report_from(params[:filename])

    erb :index
  end
end
