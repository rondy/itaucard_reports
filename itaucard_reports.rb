module ItaucardReports
  def self.parse_report_from(filename)
    CreditCardStatementReport.new(filename).call
  end

  class CreditCardStatementReport
    def initialize(filename)
      @filename = filename
    end

    def call
      OpenStruct.new(
        transactions: transactions,
        total_amount: total_amount,
        in_cash_transactions: in_cash_transactions,
        in_cash_total_amount: in_cash_total_amount,
        installments_transactions: installments_transactions,
        installments_total_amount: installments_total_amount,
        current_month_transactions: current_month_transactions,
        current_month_total_amount: current_month_total_amount,
        previous_month_transactions: previous_month_transactions,
        previous_month_total_amount: previous_month_total_amount,
        next_month_installments_transactions: next_month_installments_transactions,
        next_month_installments_total_amount: next_month_installments_total_amount,
        tim_transactions: tim_transactions,
        tim_total_amount: tim_total_amount,
        uber_transactions: uber_transactions,
        uber_total_amount: uber_total_amount,
      )
    end

    def transactions
      @transactions ||= CreditCardStatementParser.new(@filename).call
    end

    def total_amount
      calculate_total_amount(transactions)
    end

    def in_cash_transactions
      transactions
        .select { |t| t.in_cash? }
    end

    def in_cash_total_amount
      calculate_total_amount(in_cash_transactions)
    end

    def installments_transactions
      transactions
        .select { |t| !t.in_cash? }
    end

    def installments_total_amount
      calculate_total_amount(installments_transactions)
    end

    def current_month_transactions
      transactions
        .select { |t| t.current_month? }
    end

    def current_month_total_amount
      calculate_total_amount(current_month_transactions)
    end

    def previous_month_transactions
      transactions
        .select { |t| t.previous_month? }
    end

    def previous_month_total_amount
      calculate_total_amount(previous_month_transactions)
    end

    def next_month_installments_transactions
      transactions
        .select { |t| !t.in_cash? }
        .select { |t| t.installment_in_next_month? }
    end

    def next_month_installments_total_amount
      calculate_total_amount(next_month_installments_transactions)
    end

    def tim_transactions
      transactions
        .select { |t| t.tim? }
    end

    def tim_total_amount
      calculate_total_amount(tim_transactions)
    end

    def uber_transactions
      transactions
        .select { |t| t.uber? }
    end

    def uber_total_amount
      calculate_total_amount(uber_transactions)
    end

    private

    def calculate_total_amount(transactions)
      transactions
        .map { |t| t.amount }
        .inject(:+)
        .to_f
        .round(2)
    end
  end

  class CreditCardStatementParser
    def initialize(filename)
      @filename = filename
    end

    def call
      parse_raw_transactions
        .map(&build_transactions)
        .select(&ignore_credit_transactions)
    end

    private

    def parse_raw_transactions
      RawTransactionsParser.for(@filename).call
    end

    def build_transactions
      lambda { |transaction_hash| TransactionItem.new(transaction_hash, from_date: statement_date) }
    end

    def ignore_credit_transactions
      lambda { |transaction| !transaction.credit? }
    end

    def statement_date
      month, year = @filename
        .match(/\Afatura(\d{2})(\d{4})(trn)?\.html\z/)
        .to_a
        .values_at(1, 2)

      Time.parse("#{year}-#{month}-01")
    end
  end

  class RawTransactionsParser
    def self.for(filename)
      if filename =~ /trn.html\z/
        RawTransactionsTRNParser.new(filename)
      else
        self.new(filename)
      end
    end

    def initialize(filename)
      @filename = filename
    end

    def call
      statement_file.css('table#lancamento tr').map do |row|
        cols = row.css('td')

        {
          credit: is_credit_transaction?(cols),
          date: cols[0].text,
          full_title: cols[1].text,
          amount: cols[2].text
        }
      end
    end

    def statement_file
      File.open(@filename) { |f| Nokogiri::HTML(f) }
    end

    def is_credit_transaction?(cols)
      !!(cols[2].attr('style') =~ /color:#3CB371/)
    end
  end

  class RawTransactionsTRNParser < RawTransactionsParser
    def call
      statement_file
        .css('table.TRNfundo tbody tr')
        .reject { |tr| tr.css('td.TRNcampo_linha').size.zero? }
        .map do |row|
          cols = row.css('td')

          {
            credit: is_credit_transaction?(cols),
            date: cols[0].text,
            full_title: cols[1].text,
            amount: cols[2].text
          }
        end
    end

    def is_credit_transaction?(cols)
      (cols[2].text.gsub(',', '.').to_f < 0)
    end
  end

  class TransactionItem
    FULL_TITLE_PARSE_REGEXP = /\A([\s\w\-\.\'\*]+)(\d{2}\/\d{2})?\z/

    def initialize(transaction_hash, from_date: Time.now)
      @transaction_hash = transaction_hash
      @from_date = from_date
      parse_full_title!
    end

    def date
      @transaction_hash.fetch(:date)
    end

    def full_title
      @transaction_hash.fetch(:full_title).strip
    end

    def title
      @title = @title.strip if !@title.empty?
      @title
    end

    def installments
      @installments
    end

    def amount
      BigDecimal.new(@transaction_hash.fetch(:amount).gsub(',', '.'))
    end

    def credit?
      !!@transaction_hash.fetch(:credit)
    end

    def in_cash?
      installments.nil?
    end

    def current_month?
      transaction_month == @from_date.month
    end

    def previous_month?
      transaction_month == @from_date.to_date.prev_month.month
    end

    def installment_in_next_month?
      current_installment, total_installments = installments.to_s.split('/')
      current_installment.to_i < total_installments.to_i
    end

    def tim?
      !!(
        title =~ Regexp.union(
          /\ATim/,
          /\APcte Adic 300Mb/,
          /\ATIM CTRWHATSAPP/,
          /\ATIM INTERNET/
        )
      )
    end

    def uber?
      title.upcase == 'UBER*UBER'
    end

    private

    def parse_full_title!
      @title, @installments = full_title.scan(FULL_TITLE_PARSE_REGEXP).first
    end

    def transaction_month
      date.split('/').last.to_i
    end
  end
end
